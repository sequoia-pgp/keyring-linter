use std::ffi::OsString;

use clap::Parser;

/// Checks for and optionally repairs OpenPGP certificates that use
/// SHA-1.
#[derive(Parser)]
#[command(author, version, about, long_about = None, verbatim_doc_comment,
          after_help="\
`sq-keyring-linter` checks the supplied certificates for the following
SHA-1-related issues:

  - Whether a certificate revocation uses SHA-1.

  - Whether the current self signature for a non-revoked User ID uses
    SHA-1.

  - Whether the current subkey binding signature for a non-revoked,
    live subkey uses SHA-1.

  - Whether a primary key binding signature (\"backsig\") for a
    non-revoked, live subkey uses SHA-1.

Diagnostics are printed to stderr.  At the end, some statistics are
shown.  This is useful when examining a keyring.  If `--fix` is
specified and at least one issue could be fixed, the fixed
certificates are printed to stdout.

This tool does not currently support smart cards.  But, if only the
subkeys are on a smart card, this tool may still be able to partially
repair the certificate.  In particular, it will be able to fix any
issues with User ID self signatures and subkey binding signatures for
encryption-capable subkeys, but it will not be able to generate new
primary key binding signatures for any signing-capable subkeys.

EXIT STATUS:

If `--fix` is not specified:
  2  if any issues were found,
  1  if not issues were found, but there were errors reading the input,
  0  if there were no issues.

If `--fix` is specified:
  3  if any issues could not be fixed,
  1  if not issues were found, but there were errors reading the input,
  0  if all issues were fixed or there were no issues.

EXAMPLES:

  # To gather statistics, simply run:
  $ sq-keyring-linter keyring.pgp

  # To fix a key:
  $ gpg --export-secret-keys FPR | sq-keyring-linter --fix -p passw0rd -p password123 | gpg --import

  # To get a list of keys with issues:
  $ sq-keyring-linter --list-keys keyring.pgp | while read FPR; do something; done

SEE ALSO:

sq-keyring-linter's homepage: <https://gitlab.com/sequoia-pgp/keyring-linter>
")]
pub struct Linter {
    // (progn (make-local-variable 'fill-column) (setq fill-column 70))

    /// Sets the reference time to TIME.
    ///
    /// TIME is interpreted as an ISO 8601 timestamp.  To set the
    /// reference time to July 21, 2013 at midnight UTC, you can
    /// do:
    ///
    ///   $ sq-keyring-linter --time 20130721 ...
    ///
    /// To include a time, add a T, the time and optionally the
    /// timezone (the default timezone is UTC):
    ///
    ///   $ sq-keyring-linter --time 20130721T0550+0200 ...
    #[arg(short, long)]
    pub time: Option<String>,

    /// Quiet; does not output any diagnostics.
    #[arg(short, long)]
    pub quiet: bool,

    /// Attempts to fix certificates, when possible.
    #[arg(short, long)]
    pub fix: bool,

    /// When fixing a certificate, the fixed certificate is exported
    /// without any secret key material.  Using this switch causes any
    /// secret key material to also be exported.
    #[arg(short, long, verbatim_doc_comment)]
    pub export_secret_keys: bool,

    /// A key's password.  Normally this is not needed: if stdin is
    /// connected to a tty, the linter will ask for a password when
    /// needed.
    #[arg(
        short,
        long,
        verbatim_doc_comment
    )]
    pub password: Vec<String>,

    /// If set, outputs a list of fingerprints, one per line, of
    /// certificates that have issues.  This output is intended for
    /// use by scripts.
    ///
    /// This option implies "--quiet". If you also specify "--fix",
    /// errors will still be printed to stderr, and fixed certificates
    /// will still be emitted to stdout.
    #[arg(short='k', long, verbatim_doc_comment)]
    pub list_keys: bool,

    /// A list of OpenPGP keyrings to process.  If none are specified,
    /// a keyring is read from stdin.
    #[arg(verbatim_doc_comment)]
    pub file: Vec<OsString>,
}
