use std::fs;
use std::io;

use clap_complete::Shell;

fn main() {
    build_man_pages().unwrap();
    build_shell_completions().unwrap();
}

#[allow(unused)]
mod cli {
    include!("src/cli.rs");
}

fn build_man_pages() -> io::Result<()> {
    // Man page support.
    let out_dir = std::path::PathBuf::from(
        std::env::var_os("OUT_DIR").ok_or(std::io::ErrorKind::NotFound)?);

    use clap::CommandFactory;

    let man = clap_mangen::Man::new(cli::Linter::command());
    let mut buffer: Vec<u8> = Default::default();
    man.render(&mut buffer)?;

    let filename = out_dir.join("sq-keyring-linter.1");
    println!("cargo:warning=writing man page to {}", filename.display());
    std::fs::write(filename, buffer)?;

    for sc in cli::Linter::command().get_subcommands() {
        let man = clap_mangen::Man::new(sc.clone());
        let mut buffer: Vec<u8> = Default::default();
        man.render(&mut buffer)?;

        let filename = out_dir.join(format!("sq-keyring-linter-{}.1", sc.get_name()));
        println!("cargo:warning=writing man page to {}", filename.display());
        std::fs::write(filename, buffer)?;
    }

    Ok(())
}

fn build_shell_completions() -> io::Result<()> {
    let out_dir = std::path::PathBuf::from(
        std::env::var_os("OUT_DIR").ok_or(std::io::ErrorKind::NotFound)?);

    fs::create_dir_all(&out_dir).unwrap();

    use clap::CommandFactory;

    let mut sq_linter = cli::Linter::command();
    for shell in &[Shell::Bash, Shell::Fish, Shell::Zsh, Shell::PowerShell,
                   Shell::Elvish] {
        let path = clap_complete::generate_to(
            *shell, &mut sq_linter, "sq-keyring-linter", &out_dir)
            .unwrap();
        println!("cargo:warning=completion file is generated: {:?}", path);
    };

    Ok(())
}
